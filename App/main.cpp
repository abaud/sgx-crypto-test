#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <threadpool.h>

#include <set>
#include <string>
#include <ratio>
#include <chrono>
#include <dirent.h>
#include "enclavemanager.h"

using namespace std;
using namespace std::chrono;
using clock_type = std::chrono::high_resolution_clock;


void showHelpMessage(){
    printf("MixNN Enclave\n");
    printf("usage : ./app [-g] [-w <watch-path>] [-t <#-of-threads>]\n");
    printf("Watch a directory for file creation and decipher and add its content inside an SGX enclave.\n\n");
    printf("Options:\n");
    printf("\t-g : generate keys needed to encrypt file that will be added to the Enclave.\n");
    printf("\t-w <watch-path> : folder that will be watched, defaults ./batch/\n");
    printf("\t-t <#-of-threads> : set the number of thread used inside the enclave. defaults to 1\n");
    printf("\t-h show this help message\n\n");
}

//* Main Binary
int main(int argc, char const *argv[]) {
    string watchpath = "./batch/";
    bool generate = false;
    int threadCount = 1;

    for(int i = 1; i < argc; i++){
         if(!strcmp(argv[i], "-g")) {
            generate = true;
        }else if(!strcmp(argv[i], "-w")){
            i++;
            if(i >= argc){
                printf("wrong usage : a path must be given when using -w\n");
                showHelpMessage();
                return -1;
            }
            watchpath = argv[i];
        }else if(!strcmp(argv[i], "-h")){
            showHelpMessage();
            return 0;
        }else if(!strcmp(argv[i], "-t")){
            i++;
            if(i >= argc){
                printf("wrong usage : number of thread must be given when using -t\n");
                showHelpMessage();
                return 0;
            }
            threadCount = atoi(argv[i]);
            if(threadCount < 0 || threadCount > 4){
                printf("number of thread must be between 1 & 4\n");
            }
        }else{
            printf("wrong usage\n");
            showHelpMessage();
            return -1;
        }
    }

    EnclaveManager *em = EnclaveManager::getManager();
    ThreadPool *tp = new ThreadPool(threadCount);
    set<string> doneFiles;
    
    em->init(generate);
    auto interval = std::chrono::milliseconds(5000);
    auto when_started = clock_type::now(); 
    auto target_time = when_started + interval;
    
    printf("Will watch %s\n", watchpath.c_str());
    
    while(1){
        DIR *dir; struct dirent *diread;
        vector<string> files;

        if ((dir = opendir(watchpath.c_str())) != nullptr) {
            while ((diread = readdir(dir)) != nullptr) {
                if (strcmp(diread->d_name, ".") && strcmp(diread->d_name, "..") && !(doneFiles.find(diread->d_name) != doneFiles.end())){
                    files.push_back(diread->d_name);
                    doneFiles.insert(diread->d_name);
                }
            }
            closedir (dir);
        } else {
            perror ("opendir");
            return EXIT_FAILURE;
        }

        printf("found %ld files; enqueuing\n", files.size());

        for (auto file : files){
            tp->enqueue(EnclaveManager::processStatic, (watchpath + file));
        }

        std::this_thread::sleep_until(target_time);
        target_time += interval;
    }
}
