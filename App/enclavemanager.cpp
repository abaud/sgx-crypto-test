#include "enclavemanager.h"
#include <iostream>
#include <stdexcept>
#include "message/message.h"
#include <thread>

EnclaveManager* EnclaveManager::enclaveManager_ = nullptr;

EnclaveManager::EnclaveManager(){
     if (initialize_enclave(&this->global_eid, "enclave.token", "enclave.signed.so") < 0) {
        std::cout << "Fail to initialize enclave." << std::endl;
        throw std::runtime_error("Fail to initialize enclave");
    }
    printf("Enclave Initialized!\n");
}

EnclaveManager* EnclaveManager::getManager(){
    if(enclaveManager_ == nullptr){
        enclaveManager_ = new EnclaveManager();
    }
    return enclaveManager_;
}

void EnclaveManager::init(bool generate){
    initEnclave(global_eid, generate);
    this->initialized = true;
}

void EnclaveManager::process(string path){
    Message *message;
    message = new Message(path);
    
    message->loadContent();
    printf("Loaded %s ! size = %ld \n", path.c_str(), message->getLength());
    sgx_status_t st = decryptInEnclave(global_eid, message->getContent());
    message->deleteFile();
}

void EnclaveManager::processStatic(string path){
    (EnclaveManager::getManager())->process(path);
}
