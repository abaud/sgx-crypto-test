#ifndef ENCLAVEMANAGER_H_
#define ENCLAVEMANAGER_H_
#include "Enclave_u.h"
#include "sgx_utils/sgx_utils.h"
#include <string>

using namespace std;

class EnclaveManager{
    private:
        sgx_enclave_id_t global_eid = 0;
        bool initialized = false;
    protected:
        EnclaveManager();
        static EnclaveManager* enclaveManager_;
    public:
        /**
         * Singletons should not be cloneable.
         */
        EnclaveManager(EnclaveManager &other) = delete;
        /**
         * Singletons should not be assignable.
         */
        void operator=(const EnclaveManager &) = delete;
        static EnclaveManager* getManager();
        static void processStatic(string path);
        void init(bool generate);
        void process(string path);
        bool isInitialized() { return this->initialized; };
};

#endif