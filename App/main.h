#ifndef MAIN_H_
#define MAIN_H_
#include <stdint.h>
#include <stddef.h>

/***************************************************
 * config.
 ***************************************************/
#define BAG_FILE "message_bag.seal"
#define BATCH_FOLDER "./batch"
#define SHUFFLED_BAG_FILE "shuffled_bag.txt"
#define MESSAGE_FILE "message.txt"
#define MAX_MESSAGE 10

void decrypt( char *encrypted);
void create_rsa_key();

#endif // MAIN_H_