#include "ocall.h"
#include "Enclave_u.h"


#include <fstream>
#include <iostream>

using namespace std;

size_t getPrivateKeyFilesize() {
    ifstream file(PRIVATE_FILE, ios::in | ios::binary);
    if (file.fail()) {
        return 1;
    }
    const auto begin = file.tellg();
    file.seekg(0, ios::end);
    const auto end = file.tellg();
    size_t sealed_size = (end - begin);
    file.close();
    return sealed_size;
}

int loadPrivateKeyFile(uint8_t *sealed_data, size_t sealed_size) {
    ifstream file(PRIVATE_FILE, ios::in | ios::binary);
    if (file.fail()) {
        return 1;
    }
    file.read((char *)sealed_data, sealed_size);
    file.close();
    return 0;
}

int savePrivateKeyFile(const uint8_t *sealed_data, const size_t sealed_size) {
    ofstream file(PRIVATE_FILE, ios::out | ios::binary);
    if (file.fail()) {
        return 1;
    }
    file.write((const char *)sealed_data, sealed_size);
    file.close();
    return 0;
}

int savePublicKeyFile(const char *data, const size_t size) {
    ofstream file(PUBLIC_FILE, ios::out | ios::binary);
    if (file.fail()) {
        return 1;
    }
    file.write((const char *)data, size);
    file.close();
    return 0;
}

void uprint(const char *str) {
    /* Proxy/Bridge will check the length and null-terminate 
     * the input string to prevent buffer overflow. 
     */
    printf("ENCLAVE : %s", str);
    fflush(stdout);
}