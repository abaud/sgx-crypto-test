#ifndef OCALL_H_
#define OCALL_H_
#include <stdint.h>
#include <stddef.h>
#if DEBUG
#define PRIVATE_FILE "private_key_debug.seal"
#define PUBLIC_FILE "public_key_debug.pub"
#else
#define PRIVATE_FILE "private_key_release.seal"
#define PUBLIC_FILE "public_key_release.pub"
#endif
#endif // OCALL_H_