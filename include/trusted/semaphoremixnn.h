#ifndef SEMAPHORE_TRUSTED_H_
#define SEMAPHORE_TRUSTED_H_

#include <sgx_thread.h>

class Semaphore {
    sgx_thread_mutex_t mutex_;
    sgx_thread_cond_t condition_;
    unsigned long count_ = 0; // Initialized as locked.

public:
    inline Semaphore(){}
    inline Semaphore(unsigned long count){
        this->count_ = count;
        this->mutex_ = SGX_THREAD_MUTEX_INITIALIZER;
        this->condition_ = SGX_THREAD_COND_INITIALIZER;
    }

    inline ~Semaphore(){
        sgx_thread_cond_destroy(&this->condition_);
        sgx_thread_mutex_destroy(&this->mutex_);
    }
    inline void release() {
        sgx_thread_mutex_lock(&mutex_);
        ++count_;
        sgx_thread_cond_signal(&condition_);
        sgx_thread_mutex_unlock(&mutex_);
    }

    inline void acquire() {
        sgx_thread_mutex_lock(&mutex_);
        while(!count_) // Handle spurious wake-ups.
            sgx_thread_cond_wait(&condition_, &mutex_);
        --count_;
        sgx_thread_mutex_unlock(&mutex_);
    }

    inline bool try_acquire() {
        if(count_) {
            --count_;
            return true;
        }
        return false;
    }
};

#endif