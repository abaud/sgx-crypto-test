#include "crypto.h"

#include <openssl/bn.h>
#include <openssl/ec.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <sgx_thread.h>
#include "Enclave_t.h"
#include "utils/log.h"
#include "trusted/sealing.h"
#include "base64.h"
#include "sgx_tcrypto.h"
#include "sgx_tseal.h"

int next_linefeed(char *str);
int envelope_open(EVP_PKEY *evp_pkey, unsigned char *ciphertext,
                  int ciphertext_len, unsigned char *encrypted_key,
                  int encrypted_key_len, unsigned char *iv,
                  unsigned char **plaintext, int *plaintext_len, unsigned char *tag);
sgx_thread_mutex_t m = SGX_THREAD_MUTEX_INITIALIZER;

struct evp_pkey_st {
    int type;
    int save_type;
    int references;
    const EVP_PKEY_ASN1_METHOD *ameth;
    ENGINE *engine;
    union {
        char *ptr;
#ifndef OPENSSL_NO_RSA
        struct rsa_st *rsa; /* RSA */
#endif
#ifndef OPENSSL_NO_DSA
        struct dsa_st *dsa; /* DSA */
#endif
#ifndef OPENSSL_NO_DH
        struct dh_st *dh; /* DH */
#endif
#ifndef OPENSSL_NO_EC
        struct ec_key_st *ec; /* ECC */
#endif
    } pkey;
    int save_parameters;
    STACK_OF(X509_ATTRIBUTE) * attributes; /* [ 0 ] */
    CRYPTO_RWLOCK *lock;
} /* EVP_PKEY */;


int next_linefeed(char *str) {
    int count = 0;
    while(*str != '\n' && *str != '\0'){
        count++;
        str++;
    }
    return count;
}

int envelope_open(EVP_PKEY *evp_pkey, unsigned char *ciphertext,
                  int ciphertext_len, unsigned char *encrypted_key,
                  int encrypted_key_len, unsigned char *iv,
                  unsigned char **plaintext, int *plaintext_len, unsigned char *tag) {
    EVP_CIPHER_CTX *ctx;
    const EVP_CIPHER *type = EVP_aes_256_gcm();
    int len, ret = 0;
    unsigned char *tmpptxt = NULL;
    if ((ctx = EVP_CIPHER_CTX_new()) == NULL){
        printf("%s\n", ERR_error_string(ERR_get_error(), NULL));
        return 0;
    }
    if ((tmpptxt = (unsigned char *)malloc(ciphertext_len)) == NULL){
        printf("%s\n", ERR_error_string(ERR_get_error(), NULL));
        ret = 0;
        goto err;
    }    

    if (EVP_OpenInit(ctx, type, encrypted_key, EVP_PKEY_size(evp_pkey),
                     iv, evp_pkey) != 1){
        printf("%s\n", ERR_error_string(ERR_get_error(), NULL));
        ret = 0;
        goto err;
    }

    if (EVP_OpenUpdate(ctx, tmpptxt, &len, ciphertext, ciphertext_len) != 1){
        printf("%s\n", ERR_error_string(ERR_get_error(), NULL));   
        ret = 0;
        goto err;
    }

    *plaintext_len = len;

    /* Set expected tag value. Works in OpenSSL 1.0.1d and later     */
    if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag)){
        printf("%s\n", ERR_error_string(ERR_get_error(), NULL));
        ret = 0;
        goto err;
    }

    if (EVP_OpenFinal(ctx, tmpptxt + len, &len) != 1){
        printf("%s\n", ERR_error_string(ERR_get_error(), NULL));
        ret = 0;
        goto err;
    }

    *plaintext_len += len;

    *plaintext = tmpptxt;
    tmpptxt = nullptr;
    ret = 1;
err:
    EVP_CIPHER_CTX_free(ctx);
    if(ret == 0){
        free(tmpptxt);
    }
    return ret;
}

void loadPrivateKey(EVP_PKEY **evp_pkey){
    size_t sealed_size = PRIVATE_KEY_SIZE + sizeof(sgx_sealed_data_t);
    sgx_status_t ocall_status;
    uint8_t *sealed_data = (uint8_t *)malloc(sealed_size);
    int ocall_ret;
    unsigned char *iv, *enckey, *cipher, *tag, *plaintext;
    int ivlen, enckeylen, cipherlen, taglen;
    int plaintextlen;
    sgx_status_t sealing_status;
    uint32_t private_len = PRIVATE_KEY_SIZE;
    unsigned char *private_key = (unsigned char *)malloc(private_len);
    ocall_status = loadPrivateKeyFile(&ocall_ret, sealed_data, sealed_size);
    if (ocall_ret != 0 || ocall_status != SGX_SUCCESS) {
        free(sealed_data);
        return;
    }
    sealing_status = unseal((sgx_sealed_data_t *)sealed_data, sealed_size, private_key, private_len);
    free(sealed_data);
    if (sealing_status != SGX_SUCCESS) {
        free(private_key);
        return;
    }
    *evp_pkey = d2i_PrivateKey(EVP_PKEY_RSA, NULL, (const unsigned char**)&private_key, PRIVATE_KEY_SIZE);
    printf("%ld", private_len);
}

void decrypt(char* encrypted, char **decrypted, EVP_PKEY *evp_pkey){
    //load private key

    char *message = encrypted;
    //parse message
    int line_length;
    unsigned char *iv, *enckey, *cipher, *tag, *plaintext;
    int ivlen, enckeylen, cipherlen, taglen;
    int plaintextlen;

    //IV
    line_length = next_linefeed(message);
    iv = base64::unbase64(message, line_length, &ivlen);
    message += line_length + 1;
    //ENCKEY
    line_length = next_linefeed(message);
    enckey = base64::unbase64(message, line_length, &enckeylen);
    message += line_length + 1;
    //CIPHER
    line_length = next_linefeed(message);
    cipher = base64::unbase64(message, line_length, &cipherlen);
    message += line_length + 1;
    //TAGS
    line_length = next_linefeed(message);
    tag = base64::unbase64(message, line_length, &taglen);
    if(envelope_open(evp_pkey, cipher, cipherlen, enckey, enckeylen, iv, &plaintext, &plaintextlen, tag) == 0){
        printf("ERROR\n");
    }
    plaintext[plaintextlen] = '\0'; // not useful?
    free(iv);
    free(enckey);
    free(cipher);
    free(tag);
    *decrypted = (char*)(plaintext);
}

void generateRSAKeys(EVP_PKEY **evp_pkey) {
    BIGNUM *bn = BN_new();
    if (bn == NULL) {
        printf("BN_new failure: %ld\n", ERR_get_error());
        return;
    }
    int ret = BN_set_word(bn, RSA_F4);
    if (!ret) {
        printf("BN_set_word failure\n\n");
        return;
    }

    RSA *keypair = RSA_new();
    if (keypair == NULL) {
        printf("RSA_new failure: %ld\n", ERR_get_error());
        return;
    }
    ret = RSA_generate_key_ex(keypair, RSA_KEY_SIZE, bn, NULL);
    if (!ret) {
        printf("RSA_generate_key_ex failure: %ld\n", ERR_get_error());
        return;
    }

    *evp_pkey = EVP_PKEY_new();
    if (*evp_pkey == NULL) {
        printf("EVP_PKEY_new failure: %ld\n", ERR_get_error());
        return;
    }
    EVP_PKEY_assign_RSA(*evp_pkey, keypair);
    printf("RSA key pair generated ! \nSealing & Saving...\n");
    // public key - string
    int len = i2d_PublicKey(*evp_pkey, NULL);
    unsigned char *buf = (unsigned char *)malloc(len + 1);
    unsigned char *tbuf = buf;
    i2d_PublicKey(*evp_pkey, &tbuf);
    sgx_status_t ocall_status;
    int ocall_ret;
    ocall_status = savePublicKeyFile(&ocall_ret, (const char*)buf, len);
    free(buf);

    // private key - string
    len = i2d_PrivateKey(*evp_pkey, NULL);
    buf = (unsigned char *)malloc(len + 1);
    tbuf = buf;
    i2d_PrivateKey(*evp_pkey, &tbuf);
    sgx_status_t status;
    size_t sealed_size_private = sizeof(sgx_sealed_data_t) + len + 1;
    uint8_t *sealed_private_key = (uint8_t *)malloc(sealed_size_private);
    status = seal(buf, len + 1,
                  (sgx_sealed_data_t *)sealed_private_key, sealed_size_private);
    if (status != SGX_SUCCESS) {
        printf("Sealing Failed :(\n");
    }

    ocall_status = savePrivateKeyFile(&ocall_ret, sealed_private_key, sealed_size_private);
    free(buf);
    BN_free(bn);
    return;
}
