#include "Enclave.h"

#include <openssl/evp.h>
#include "Enclave_t.h"
#include "crypto/crypto.h"

EVP_PKEY *evp_pkey;

void initEnclave(int generate){
    if(generate){
        printf("generate new key..\n");
        generateRSAKeys(&evp_pkey);
    }else{
        printf("Loading Key..\n");
        loadPrivateKey(&evp_pkey);
    }
   
    printf("Key loaded !\n");
}

void decryptInEnclave(char *encrypted) {
    char *message;
    decrypt(encrypted, &message, evp_pkey);
    printf("decrypted !\n");
    free(message);
}

void destroyEnclave(){
    printf("Cleaning.\n\n");
     EVP_PKEY_free(evp_pkey);
}
