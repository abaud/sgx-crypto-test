# SGX Crypto Test
## Requirements
SGX sdk & intel-sgx-ssl are needed to build the app.

## Build

```
make
```
build all necessary files. The SGX app is built in Hardware debug mode by default.
- `SGX_MODE=SIM` can be set to build in simulation
- `SGX_DEBUG=0` to build in release mode

## Usage
### Setup 
you first need to build and generate the RSA keys :
- build the project
- run `./app -g` to generate the RSA keys.
- `openssl rsa -RSAPublicKey_in -in public_key_debug.pub -inform DER -outform PEM -out pubkey.pem` to change the public key format that will be used by `encrypt`
    - if app is built in release mode the public key is saved at `public_key_release.pub` so one should use: 
    ```
    openssl rsa -RSAPublicKey_in -in public_key_release.pub -inform DER -outform PEM -out pubkey.pem
    ```
- create the `batch/` directory, which is the default directory watched by the app.

each subsequent call to `./app` without the `-g` flag will now load the previously created private key.

### Running
- if sgx app isn't running, start it (`./app`)
- create the `example.txt` file and fill it with some text.
- run `./encrypt > encrypted.txt`
- run `./clcopy 1000` it will copy `encrypted.txt` inside the `./batch/` folder every second.

`./app` should now decrypt every copied file.  it checks for new files inside the `./batch/` folder every 5 seconds.

## Problems

If `./app` is run with only 1 thread the decryption works properly.<br>
running it with multiple threads (e.g. `./app -t 2`) in hardware mode, gives 
```
error:0407109F:rsa routines:RSA_padding_check_PKCS1_type_2:pkcs decoding error
```
everything works properly if `app` is built in simulation release mode ( couldn't test hardware release mode ).<br>
problem seems to be related to openssl locks usage (https://www.openssl.org/blog/blog/2017/02/21/threads/) that probably can't find which lock API to use and default to their `threads_none` which leads to concurrent usage of the key without locking mechanism. <br>
adding a lock around `EVP_OpenInit()` in `crypto/crypto.c` fix the problem but probably affect the performance more than having the locking mechanism handled entirely by the openssl library.