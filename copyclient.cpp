#include <chrono>
#include <thread>
#include <iostream>
#include <filesystem>
#include <string>
namespace fs = std::filesystem;

using namespace std::literals;
using clock_type = std::chrono::high_resolution_clock;

void copyFile();
double w = 4;
int main(int argc, char *argv[])
{
    if(argc < 2){
        printf("usage ./clcopy <interval_in_seconds>; interval is integer in milliseconds\n");
    }
    auto interval = std::chrono::milliseconds(strtol(argv[1], nullptr, 10));
    auto when_started = clock_type::now(); 
    auto target_time = when_started + interval;
    for (int i = 0; i < 10000; i++) {
        copyFile();
        std::this_thread::sleep_until(target_time);
        target_time += interval;
    }
    auto now = clock_type::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(now - when_started).count() << "ms\n";
}

void copyFile(){
    auto now = clock_type::now();
    std::string filename = "./batch/example";
    filename += "" + std::to_string(now.time_since_epoch().count());
    std::cout << filename << std::endl;
    fs::copy("encrypted.txt", filename);
}